$(function(){
	if($.browser.msie){
		alert('Esse sistema é muito moderno para o Internet Explorer!\nAcesse pelo Mozilla Firefox ou Google Chrome.');
		window.location = "http://www.kombizita.net/"

	}


	$("#start").click();


	var $dropActive;



	function drag ($selector){
		$($selector).drag("start",function(){
				if($(this)[0].tagName == "LI"){
					$top =  $(this).offsetParent().top;
					$left =  $(this).offsetParent().left;
					$(this).css({

						top: $top,
						left: $left
					})
				}
				return $( this ).addClass('grabbing').clone()
					.css({"opacity": .75, 'position': 'absolute'} )
					.appendTo( document.body );
			})
			.drag(function( ev, dd ){
				//$( this ).css({'position': 'absolute'});
				$( dd.proxy ).addClass('active').css({
					top: dd.offsetY,
					left: dd.offsetX
				});
			})
			.drag("end",function( ev, dd ){



				$( this ).css({'position': 'absolute'}).animate({
					top: dd.offsetY,
					left: dd.offsetX,
					bottom: 'auto',
					right: 'auto'
				}, 420 , function(){
					if(dd.drop.length){
						var $newItem;
						$thisList = $dropActive.find('ul');


						if($(this).hasClass('card')){
							$newItem = $('<li class="drag fade" >'+ $(this).find('header h2').text()+ '<span><a href="#" class="lsf description" title="'+ $(this).find('header p').text()+'">info</a></li>').appendTo($thisList);
						} else {
							$newItem = $('<li class="drag fade" >'+ $(this).html()+'</li>').appendTo($thisList);
						}

						drag($newItem);

						$thisList.children().removeClass('fade')

						$("[title]:not(.lsf-icon)").tipTip();

						$(this).fadeOut(function(){
							$(this).remove();
						});
					}
				}).removeClass('grabbing');



				$( dd.proxy ).remove();
			});
	}
	$(".drop")
		.drop("start",function(){
			$( this ).addClass("active");
		})
		.drop("end",function(){
			$( this ).removeClass("active");
			$dropActive = $(this);
		});



	drag('.drag');

	$('footer a.help').on('click', function(){
		$(this).toggleClass('active');
		$('#ajuda').toggleClass('fade')
	});


	$('#finish').on('click', function(event) {
		event.preventDefault();
		var $cardsLength = $('#cards').find('div').length;
		if($cardsLength>0){
			$text = ($cardsLength==1)? 'Você possui <strong>1 cartão</strong> sem ordenar, deseja continuar?':'Você possui <strong>'+$cardsLength+' cartões</strong> sem ordenar, deseja continuar?'
			$('#confirm p').html($text);
		  	$('#confirm').reveal({
			     closeonbackgroundclick: false,
			     dismissmodalclass: "close-modal"
			});
		} else {
			$('#thank').reveal({
			     closeonbackgroundclick: false,
			     dismissmodalclass: "close-modal"
			});
		}
	});

	/**
	 * Correção efeito cartão
	 */
	$("#cards div").each(function(){
		$(this).css({
			top: $(this).offset().top,
			left: $(this).offset().left
		})
	});




	/**
	 * TOOLTIP
	 */
	$("[title]:not(.lsf-icon)").tipTip();


	/**
	 * TEMAS
	 */
	$("#themes a").on('click', function(){
		$theme = $(this).attr('class').split('lsf ')[1];

		$('body').attr('class', '').addClass($theme)
	})

	$('footer').hover(function(){
		$("#themes").removeClass('hidden').fadeIn();
	}, function(){
		$("#themes").stop(true, true).fadeOut();

	})

	//
	/*
	$('.postit li .remove').on('click', function(){
		var $li = $(this).parents('li');
		var $title = $li.text();
		var $description = $li.find('.desctiption').attr('title');

		$("#cards").append('<div class="postit card drag"  draggable="true"><header><h2>'+$title+'</h2><p>'+$description+'</p></header></div>')

		dragANDdrop();
		//animação
		$(this).parents('li').addClass('fade').delay(200).css('overflow', 'hidden').slideUp('normal', function(){$(this).remove()})
	})
	*/
});
